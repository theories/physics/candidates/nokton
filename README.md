# theories/physics/candidates/nokton

## About this project

Nokton project or nokton theory is a concrete case of [nokton philosophy](https://gitlab.com/theories/philosophies/nokton). Here the sets are well defined. The project contains many definitions and studies (some are under construction and waiting [contributions](#contributing)). In nokton theory the nature is based on 10 dimensions (3 for space, 6 for probabilities and 1 for time).

We add two constants :

- The electric coupling constant $`H_e ∈ \mathbb{Q}_+`$.
- The gravitational coupling constant $`H_g ∈ \mathbb{Q}_+`$.

Nokton theory can be a [theory of everything](https://gitlab.com/theories/physics/toe).

## Nokton

In nokton theory we assumes that nokton automaton (see nokton philosophy for [general definition](https://gitlab.com/theories/philosophies/nokton/blob/master/README.md)) is composed by non empty finite collection of noktons. A nokton is :

- An element of $`\mathbb{Z}^3`$.
- An element of $`\{p ∈ \mathbb{Q}_+^6 | \sum_{i=1}^6 [p]_i ≤ 1\}`$.
- An element of the charges set $`C=\{c_+,c_-,c_0\}`$.

We note :

- $`Δ=\{a^+_x,a^-_x,a^+_y,a^-_y,a^+_z,a^-_z,a_0\}`$.
- $`Z_3=\mathbb{Z}^3`$.
- $`Q_6=\{p ∈ \mathbb{Q}_+^6 | \sum_{i=1}^6 [p]_i ≤ 1\}`$.

## Nokton automaton

For given $`N ∈ \mathbb{N^*}`$ and $`N`$ noktons, we note $`c^* ∈ C^N`$ the element represents the charges of these $`N`$ noktons.

In nokton theory a nokton automaton is a 5-tuple $`(N,s^*,p^*,c^*,λ_f)`$ where (see nokton philosophy for notations)  :

- The actions set $`A`$ is the set $`Δ^N`$.
- The external states set $`S`$ is the set $`Z_3^N`$, it will be named positions set.
- The internal states set $`P`$ is the set $`Q_6^N`$, it will be named pulses set.
- The external transitions function $`λ_{out}`$ is the function defined in [this subsection](#positions-transition).
- $`λ_f:\mathbb{N^*}×Z_3×C↦\mathbb{Q}_+^6`$. This function will be named field.
- The internal transitions function $`λ_{in}`$ is the function defined in [this subsection](#pulses-transition).
- The actions distribution $`π`$ is the function defined in subsection [this subsection](#actions-distribution).

### Positions transition

For given $`(a,s) ∈ A×S`$ the positions transition is the function $`λ_{out}`$ defined as $`∀`$ $`1 ≤ i ≤ N`$, $`[λ_{out}(a,s)]_i=[s]_i+τ_{out}([a]_i)`$, where the position contribution $`τ_{out}`$ is the function

$`\begin{aligned}
   τ_{out}:Δ   &↦ Z_3 \\
   α           &↦ \begin{cases}
                     (1,0,0) &\text{if } α=a^+_x \\
                     (-1,0,0) &\text{if } α=a^-_x \\
                     (0,1,0) &\text{if } α=a^+_y \\
                     (0,-1,0) &\text{if } α=a^-_y \\
                     (0,0,1) &\text{if } α=a^+_z \\
                     (0,0,-1) &\text{if } α=a^-_z \\
                     (0,0,0) &\text{if } α=a_0
                  \end{cases}
\end{aligned}`$

### Fields

Some remarkable fields are described in this [document](content/fields.md).

### Pulses transition

The pulses transition is described in this [document](content/pulses-transition.md).

### Actions distribution

For given $`(p,a) ∈ P×A`$ the actions distribution is the function $`π`$ defined as $`\prod_{i=1}^N τ_a([p]_i,[a]_i)`$, where $`τ_{a}`$ is the function

$`\begin{aligned}
   τ_{a}:Q_6×Δ &↦ \mathbb{Q_+} \\
   (e,α)       &↦ \begin{cases}
                        [e]_1 &\text{if } α=a^+_x \\
                        [e]_2 &\text{if } α=a^-_x \\
                        [e]_3 &\text{if } α=a^+_y \\
                        [e]_4 &\text{if } α=a^-_y \\
                        [e]_5 &\text{if } α=a^+_z \\
                        [e]_6 &\text{if } α=a^-_z \\
                        1-\sum_{i=1}^6 [e]_i &\text{if } α=a_0
                     \end{cases}
\end{aligned}`$

## Contextual nokton automaton

We define two remarkable contextual nokton automaton :

- 2D contextual nokton automaton (see this [document](content/2d-context.md) for more details).
- 1D contextual nokton automaton (see this [document](content/1d-context.md) for more details).

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md) file for more details.

## Licenses

Any project content in this project is under [**Creative Commons LICENSE**](LICENSE.txt) and any project code content in this project is under [**MIT LICENSE**](LICENSE-CODE.txt).
