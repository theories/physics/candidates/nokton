# 2D reduced pulses transition

We define these functions :

$`\begin{aligned}
   \bar{ϵ^*}:Z_2  &↦ \mathbb{N} \\
   (x,y)          &↦ \begin{cases}
                        1 &\text{if } x=0 \text{ and } y=0 \\
                        x^2+y^2 &\text{else}
                     \end{cases}
\end{aligned}`$

$`\begin{aligned}
   K^*:\mathbb{Q}_+^4  &↦ \mathbb{Q}_+ \\
   e                   &↦ \sum_{i=1}^4 [e]_i
\end{aligned}`$

## 2D reduced electric coupling

We define 2D reduced electric coupling $`c_e^*`$ as :

$`\begin{aligned}
   c_e^*:Z_2×C×Z_2×C &↦ \mathbb{Q}_+^4 \\
   (r_1,c_1,r_2,c_2) &↦ \frac{H_e}{\bar{ϵ^*}(r_1-r_2)}
                        (\hat{ϵ}(\bar{c_1}\bar{c_2}[r_2-r_1]_1),\hat{ϵ}(\bar{c_1}\bar{c_2}[r_1-r_2]_1),
                        \hat{ϵ}(\bar{c_1}\bar{c_2}[r_2-r_1]_2),\hat{ϵ}(\bar{c_1}\bar{c_2}[r_1-r_2]_2))
\end{aligned}`$

## 2D reduced gravitational coupling

We define 2D reduced gravitational coupling $`c_g^*`$ as :

$`\begin{aligned}
   c_g:Z_2×Z_2 &↦ \mathbb{Q}_+^4 \\
   (r_1,r_2)   &↦ \frac{H_g}{\bar{ϵ^*}(r_1-r_2)}
                  (\hat{ϵ}([r_1-r_2]_1),\hat{ϵ}([r_2-r_1]_1),
                  \hat{ϵ}([r_1-r_2]_2),\hat{ϵ}([r_2-r_1]_2))
\end{aligned}`$

## 2D reduced field

We define 2D reduced field $`λ_f^*`$ as :

$`\begin{aligned}
   λ_f^*:\mathbb{N^*}×Z_2×C &↦ \mathbb{Q}_+^4 \\
   (t,r,c)                  &↦ κ_{2p}(λ_f(t,r,c))
\end{aligned}`$

## 2D reduced pulses transition function

We note $`K^*(e)=\lvert{e}\rvert`$.

For given $`(t,s,p) ∈ \mathbb{N^*}×\bar{S}×\bar{P}`$, the 2D reduced pulses transition is the function $`λ_{in}^*`$ defined as $`∀`$ $`1 ≤ i ≤ N`$

$`[λ_{in}^*(t,s,p)]_i=\frac{1}{1+\lvert{τ_{in}^*(t,s,c^*,[s]_i,[c^*]_i)}\rvert}([p]_i+τ_{in}^*(t,s,c^*,[s]_i,[c^*]_i))`$

where the 2D reduced pulse contribution $`τ_{in}^*`$ is the function

$`\begin{aligned}
   τ_{in}^*:\mathbb{N^*}×\bar{S}×C^N×Z_2×C  &↦ \mathbb{Q}_+^4 \\
   (t,s,q,r^*,q^*)                          &↦ λ_f^*(t,r^*,q^*)+\sum_{i=1}^N c_e^*([s]_i,[q]_i,r^*,q^*)+c_g^*([s]_i,r^*)
\end{aligned}`$

***
[2D contextual nokton automaton](../2d-context.md)
