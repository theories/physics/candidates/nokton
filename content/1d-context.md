# 1D contextual nokton automaton

For given nokton automaton $`(N,s^*,p^*,c^*,λ_f)`$. We note :

- $`Δ^*=\{a^+_x,a^-_x,a_0\} ⊂ Δ`$.
- $`Q_2=\{p ∈ Q_6 \text{ | } [p]_3=[p]_4=[p]_5=[p]_6=0\}`$.
- $`(y_0,z_0) ∈ \mathbb{Z}^2`$ a given ordered numbers pair.
- $`A^*`$ the subset $`\{a ∈ A\ |\ ∀ \ 1 ≤ i ≤ N, [a]_i ∈ Δ^*\}`$.
- $`\bar{A}`$ the set $`(Δ^*)^N`$.
- $`S^*`$ the subset $`\{s ∈ S\ |\ ∀ \ 1 ≤ i ≤ N, [[s]_i]_2=y_0 \text{ and } [[s]_i]_3=z_0\}`$.
- $`\bar{S}`$ the set $`\mathbb{Z}^N`$.
- The subset $`P^*=\{p ∈ P\ |\ ∀ \ 1 ≤ i ≤ N, [p]_i ∈ Q_2\}`$.
- $`\bar{P}`$ the set $`Q_2^N`$.

In this [document](1d-context/contextual-proof.md), we prove that if $`s^* ∈ S^*`$, $`p^* ∈ P^*`$ and $`∀`$ $`(t,s,c) ∈ \mathbb{N^*}×Z_3×C`$ $`λ_f(t,s,c) ∈ Q_2`$ the nokton automaton $`(N,s^*,p^*,c^*,λ_f)`$ is contextual.

## Reduced forms

The purpose of the reduced forms is to simplify the calculation for 1D contextual nokton.
Before using reduced forms we apply :

- To $`s^*`$, the 1D contextual position transformation $`κ_{1s}`$ defined as
  
   $`\begin{aligned}
      κ_{1s}:Z_3  &↦ \mathbb{Z} \\
      (x,y,z)     &↦ x
   \end{aligned}`$

- To $`p^*`$, the 1D contextual pulse transformation $`κ_{1p}`$ defined as
  
   $`\begin{aligned}
      κ_{1p}:\mathbb{Q}_+^6 &↦ \mathbb{Q}_+^2 \\
      p                     &↦ ([p]_1,[p]_2)
   \end{aligned}`$

After using reduced forms we apply a reverse transformation to the result using :

- 1D contextual reverse position transformation $`\bar{κ_{1s}}`$ defined as
  
   $`\begin{aligned}
      \bar{κ_{1s}}:\mathbb{Z}   &↦ Z_3 \\
      x                         &↦ (x,y_0,z_0)
   \end{aligned}`$

- 1D contextual reverse pulse transformation $`\bar{κ_{1p}}`$ defined as
  
   $`\begin{aligned}
      \bar{κ_{1p}}:\mathbb{Q}_+^2   &↦ \mathbb{Q}_+^6 \\
      p                             &↦ ([p]_1,[p]_2,0,0,0,0)
   \end{aligned}`$

## 1D reduced positions transition

For given $`(a,s) ∈ \bar{A}×\bar{S}`$, the 1D reduced positions transition is the function $`λ_{out}^*`$ defined as $`∀`$ $`1 ≤ i ≤ N`$, $`[λ_{out}^*(a,s)]_i=[s]_i+τ_{out}^*([a]_i)`$, where the 1D reduced position contribution $`τ_{out}^*`$ is the function

$`\begin{aligned}
   τ_{out}^*:Δ^*  &↦ \mathbb{Z} \\
   α              &↦ \begin{cases}
                        1 &\text{if } α=a^+_x \\
                        -1 &\text{if } α=a^-_x \\
                        0 &\text{if } α=a_0
                     \end{cases}
\end{aligned}`$

## 1D reduced pulses transition

The 1D reduced pulses transition is described in this [document](1d-context/pulses-transition.md).

## 1D reduced actions distribution

For given $`(p,a) ∈ \bar{P}×\bar{A}`$, the 1D reduced actions distribution is the function $`π^*`$ defined as $`\prod_{i=1}^N τ_a^*([p]_i,[a]_i)`$, where $`τ_{a}^*`$ is the function

$`\begin{aligned}
   τ_{a}^*:Q_2×Δ^*   &↦ \mathbb{Q}_+ \\
   (e,α)             &↦ \begin{cases}
                           [e]_1 &\text{if } α=a^+_x \\
                           [e]_2 &\text{if } α=a^-_x \\
                           1-([e]_1+[e]_2) &\text{if } α=a_0
                        \end{cases}
\end{aligned}`$

***
[Nokton theory](../README.md)
