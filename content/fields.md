# Fields

Here are some remarkable fields :

- A **steady field** is a field $`λ_f`$ defined as for given $`(t,t') ∈ \mathbb{N^*}×\mathbb{N^*}`$ such as $`t \mathrlap{\,/}{=} t'`$, $`∀`$ $`(s,c) ∈ Z_3×C`$ $`λ_f(t,s,c)=λ_f(t',s,c)`$. For given $`t_0 ∈ \mathbb{N^*}`$, the reduced form $`\bar{λ_f}`$ is the function $`Z_3×C↦Q_6`$ defined as $`∀`$ $`(s,c) ∈ Z_3×C`$ $`\bar{λ_f}(s,c)=λ_f(t_0,s,c)`$.
- A **uniform field** is a steady field $`\bar{λ_f}`$ defined as for given $`(s,s') ∈ Z_3×Z_3`$ such as $`s \mathrlap{\,/}{=} s'`$, $`∀`$ $`c ∈ C`$ $`\bar{λ_f}(s,c)=\bar{λ_f}(s',c)`$. For given $`s_0 ∈ Z_3`$, the reduced form $`\hat{λ_f}`$ is the function $`C↦Q_6`$ defined as $`∀`$ $`c ∈ C`$ $`\hat{λ_f}(c)=\bar{λ_f}(s_0,c)`$.
- A **null field** is a uniform field $`\hat{λ_f}`$ such as $`∀`$ $`c ∈ C`$ $`\hat{λ_f}(c)=(0,0,0,0,0,0)`$. A nokton automaton with null field is a natural nokton automaton.

***
[Nokton theory](../README.md)
