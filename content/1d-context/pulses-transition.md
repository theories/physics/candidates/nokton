# 1D reduced pulses transition

We define these functions :

$`\begin{aligned}
   \bar{ϵ^*}:\mathbb{Z} &↦ \mathbb{N} \\
   x                    &↦ \begin{cases}
                              1 &\text{if } x=0 \\
                              x^2 &\text{else}
                           \end{cases}
\end{aligned}`$

$`\begin{aligned}
   K^*:\mathbb{Q}_+^2   &↦ \mathbb{Q}_+ \\
   e                    &↦ [e]_1 + [e]_2
\end{aligned}`$

## 1D reduced electric coupling

We define 1D reduced electric coupling $`c_e^*`$ as :

$`\begin{aligned}
   c_e^*:\mathbb{Z}×C×\mathbb{Z}×C  &↦ \mathbb{Q}_+^2 \\
   (r_1,c_1,r_2,c_2)                &↦ \frac{H_e}{\bar{ϵ^*}(r_1-r_2)}
                                       (\hat{ϵ}(\bar{c_1}\bar{c_2}[r_2-r_1]_1),\hat{ϵ}(\bar{c_1}\bar{c_2}[r_1-r_2]_1))
\end{aligned}`$

## 1D reduced gravitational coupling

We define 1D reduced gravitational coupling $`c_g^*`$ as :

$`\begin{aligned}
   c_g:\mathbb{Z}×\mathbb{Z}  &↦ \mathbb{Q}_+^2 \\
   (r_1,r_2)                  &↦ \frac{H_g}{\bar{ϵ^*}(r_1-r_2)}
                                 (\hat{ϵ}([r_1-r_2]_1),\hat{ϵ}([r_2-r_1]_1))
\end{aligned}`$

## 1D reduced field

We define 1D reduced field $`λ_f^*`$ as :

$`\begin{aligned}
   λ_f^*:\mathbb{N^*}×\mathbb{Z}×C  &↦ \mathbb{Q}_+^2 \\
   (t,r,c)                          &↦ κ_{1p}(λ_f(t,r,c))
\end{aligned}`$

## 1D reduced pulses transition function

We note $`K^*(e)=\lvert{e}\rvert`$.

For given $`(t,s,p) ∈ \mathbb{N^*}×\bar{S}×\bar{P}`$, the 1D reduced pulses transition is the function $`λ_{in}^*`$ defined as $`∀`$ $`1 ≤ i ≤ N`$

$`[λ_{in}^*(t,s,p)]_i=\frac{1}{1+\lvert{τ_{in}^*(t,s,c^*,[s]_i,[c^*]_i)}\rvert}([p]_i+τ_{in}^*(t,s,c^*,[s]_i,[c^*]_i))`$

where the 1D reduced pulse contribution $`τ_{in}^*`$ is the function

$`\begin{aligned}
   τ_{in}^*:\mathbb{N^*}×\bar{S}×C^N×\mathbb{Z}×C   &↦ \mathbb{Q}_+^2 \\
   (t,s,q,r^*,q^*)                                  &↦ λ_f^*(t,r^*,q^*)+\sum_{i=1}^N c_e^*([s]_i,[q]_i,r^*,q^*)+c_g^*([s]_i,r^*)
\end{aligned}`$

***
[1D contextual nokton automaton](../1d-context.md)
