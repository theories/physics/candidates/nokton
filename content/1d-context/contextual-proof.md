# 1D contextual proof

## External state stability

For given $`(a,s) ∈ A^*×S^*`$, $`∀`$ $`1 ≤ i ≤ N`$ $`[λ_{out}(a,s)]_i=[s]_i+τ_{out}([a]_i)`$ where

$`\begin{aligned}
   τ_{out}:Δ   &↦ Z_3 \\
   α           &↦ \begin{cases}
                     (1,0,0) &\text{if } α=a^+_x \\
                     (-1,0,0) &\text{if } α=a^-_x \\
                     (0,1,0) &\text{if } α=a^+_y \\
                     (0,-1,0) &\text{if } α=a^-_y \\
                     (0,0,1) &\text{if } α=a^+_z \\
                     (0,0,-1) &\text{if } α=a^-_z \\
                     (0,0,0) &\text{if } α=a_0
                  \end{cases}
\end{aligned}`$

because $`a ∈ A^*`$ $`[τ_{out}([a]_i)]_2=0`$ and $`[τ_{out}([a]_i)]_3=0`$ so $`[[λ_{out}(a,s)]_i]_2=[[s]_i]_2+0=y_0`$ and $`[[λ_{out}(a,s)]_i]_3=[[s]_i]_3+0=z_0`$ ⟹ $`λ_{out}(a,s) ∈ S^*`$.

## Internal state stability

For given $`(r_1,r_2) ∈ Z_3^2`$, if $`[r_1]_2=y_0`$, $`[r_2]_2=y_0`$, $`[r_1]_3=z_0`$ and $`[r_2]_3=z_0`$ then $`[r_1 - r_2]_2=[r_2 - r_1]_2=0`$ and $`[r_1 - r_2]_3=[r_2 - r_1]_3=0`$ ⟹

If $`(c_1,c_2) ∈ C^2`$ then $`[c_e(r_1,c_1,r_2,c_2)]_3=[c_e(r_1,c_1,r_2,c_2)]_4=0`$, $`[c_e(r_1,c_1,r_2,c_2)]_5=[c_e(r_1,c_1,r_2,c_2)]_6=0`$, $`[c_g(r_1,r_2)]_3=[c_g(r_1,r_2)]_4=0`$ and $`[c_g(r_1,r_2)]_5=[c_g(r_1,r_2)]_6=0`$.

For given $`(t,s,p) ∈ \mathbb{N^*}×S^*×P^*`$, $`∀`$ $`1 ≤ i ≤ N`$ $`[λ_{in}(t,s,p)]_i=\frac{1}{1+\lvert{τ_{in}(t,s,c^*,[s]_i,[c^*]_i)}\rvert}([p]_i+τ_{in}(t,s,c^*,[s]_i,[c^*]_i))`$ where

$`p ∈ P^*`$ ⟹ $`∀`$ $`1 ≤ i ≤ N`$ $`[p_i]_3=[p_i]_4=[p_i]_5=[p_i]_6=0`$.

$`\begin{aligned}
   τ_{in}:\mathbb{N^*}×S×C^N×Z_3×C  &↦ \mathbb{Q}^6 \\
   (t,s,q,r^*,q^*)                  &↦ λ_f(t,r^*,q^*)+\sum_{i=1}^N c_e([s]_i,[q]_i,r^*,q^*)+c_g([s]_i,r^*)
\end{aligned}`$

$`p ∈ P^*`$ ⟹ $`∀`$ $`1 ≤ i ≤ N`$ $`[p_i]_3=[p_i]_4=[p_i]_5=[p_i]_6=0`$.

Because $`s ∈ P^*`$ and $`∀`$ $`(t,s,c) ∈ \mathbb{N^*}×Z_3×C`$ $`λ_f(t,s,c) ∈ Q_2`$ so $`∀`$ $`1 ≤ i ≤ N`$ $`[τ_{in}(t,s,c^*,[s]_i,[c^*]_i)]_3=[τ_{in}(t,s,c^*,[s]_i,[c^*]_i)]_4=[τ_{in}(t,s,c^*,[s]_i,[c^*]_i)]_5=[τ_{in}(t,s,c^*,[s]_i,[c^*]_i)]_6=0`$ ⟹

$`∀`$ $`1 ≤ i ≤ N`$ $`[[λ_{in}(t,s,p)]_i]_3=[[λ_{in}(t,s,p)]_i]_4=[[λ_{in}(t,s,p)]_i]_5=[[λ_{in}(t,s,p)]_i]_6=0`$ ⟹ $`λ_{in}(t,s,p) ∈ P^*`$.

## Actions distribution

For given $`(p,a) ∈ P^*×(A\setminus A^*)`$, $`π(p,a)=\prod_{i=1}^N τ_a([p]_i,[a]_i)`$ where

$`\begin{aligned}
   τ_{a}:Q_6×Δ &↦ \mathbb{Q} \\
   (e,α)       &↦ \begin{cases}
                        [e]_1 &\text{if } α=a^+_x \\
                        [e]_2 &\text{if } α=a^-_x \\
                        [e]_3 &\text{if } α=a^+_y \\
                        [e]_4 &\text{if } α=a^-_y \\
                        [e]_5 &\text{if } α=a^+_z \\
                        [e]_6 &\text{if } α=a^-_z \\
                        1-\sum_{i=1}^6 [e]_i &\text{if } α=a_0
                     \end{cases}
\end{aligned}`$

Because $`p ∈ P^*`$, $`∀`$ $`1 ≤ i ≤ N`$ $`[p_i]_3=[p_i]_4=0`$ and $`[p_i]_5=[p_i]_6=0`$. And because $`a ∈ A\setminus A^*`$, $`∃ \ 1 ≤ j ≤ N \ | \ [a]_j ∈ \{a^+_y, a^-_y, a^+_z, a^-_z\}`$ ⟹ $`π(p,a)=τ_a([p]_j,[a]_j)\prod_{i\not =j}^N τ_a([p]_i,[a]_i)=0`$.

***
[1D contextual nokton automaton](../1d-context.md)
