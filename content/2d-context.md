# 2D contextual nokton automaton

For given nokton automaton $`(N,s^*,p^*,c^*,λ_f)`$, we note :

- $`Δ^*=\{a^+_x,a^-_x,a^+_y,a^-_y,a_0\} ⊂ Δ`$.
- $`Z_2=\mathbb{Z}^2`$.
- $`Q_4=\{p ∈ Q_6 \text{ | } [p]_5=[p]_6=0\}`$.
- $`z_0 ∈ \mathbb{Z}`$ a given number.
- $`A^*`$ the subset $`\{a ∈ A\ |\ ∀ \ 1 ≤ i ≤ N, [a]_i ∈ Δ^*\}`$.
- $`\bar{A}`$ the set $`(Δ^*)^N`$.
- $`S^*`$ the subset $`\{s ∈ S\ |\ ∀ \ 1 ≤ i ≤ N, [[s]_i]_3=z_0\}`$.
- $`\bar{S}`$ the set $`Z_2^N`$.
- The subset $`P^*=\{p ∈ P\ |\ ∀ \ 1 ≤ i ≤ N, [p]_i ∈ Q_4\}`$.

In this [document](2d-context/contextual-proof.md), we prove that if $`s^* ∈ S^*`$, $`p^* ∈ P^*`$ and $`∀`$ $`(t,s,c) ∈ \mathbb{N^*}×Z_3×C`$ $`λ_f(t,s,c) ∈ Q_4`$ the nokton automaton $`(N,s^*,p^*,c^*,λ_f)`$ is contextual.

## Reduced forms

The purpose of the reduced forms is to simplify the calculation for 2D contextual nokton.
Before using reduced forms we apply :

- To $`s^*`$, the 2D contextual position transformation $`κ_{2s}`$ defined as
  
   $`\begin{aligned}
      κ_{2s}:Z_3  &↦ Z_2 \\
      (x,y,z)     &↦ (x,y)
   \end{aligned}`$

- To $`p^*`$, the 2D contextual pulse transformation $`κ_{2p}`$ defined as
  
   $`\begin{aligned}
      κ_{2p}:\mathbb{Q}_+^6 &↦ \mathbb{Q}_+^4 \\
      p                     &↦ ([p]_1,[p]_2,[p]_3,[p]_4)
   \end{aligned}`$

After using reduced forms we apply a reverse transformation to the result using :

- 2D contextual reverse position transformation $`\bar{κ_{2s}}`$ defined as
  
   $`\begin{aligned}
      \bar{κ_{2s}}:Z_2  &↦ Z_3 \\
      (x,y)             &↦ (x,y,z_0)
   \end{aligned}`$

- 2D contextual reverse pulse transformation $`\bar{κ_{2p}}`$ defined as
  
   $`\begin{aligned}
      \bar{κ_{2p}}:\mathbb{Q}_+^4   &↦ \mathbb{Q}_+^6 \\
      p                             &↦ ([p]_1,[p]_2,[p]_3,[p]_4,0,0)
   \end{aligned}`$

## 2D reduced positions transition

For given $`(a,s) ∈ \bar{A}×\bar{S}`$, the 2D reduced positions transition is the function $`λ_{out}^*`$ defined as $`∀`$ $`1 ≤ i ≤ N`$, $`[λ_{out}^*(a,s)]_i=[s]_i+τ_{out}^*([a]_i)`$, where the 2D reduced position contribution $`τ_{out}^*`$ is the function

$`\begin{aligned}
   τ_{out}^*:Δ^*  &↦ Z_2 \\
   α              &↦ \begin{cases}
                        (1,0) &\text{if } α=a^+_x \\
                        (-1,0) &\text{if } α=a^-_x \\
                        (0,1) &\text{if } α=a^+_y \\
                        (0,-1) &\text{if } α=a^-_y \\
                        (0,0) &\text{if } α=a_0
                     \end{cases}
\end{aligned}`$

## 2D reduced pulses transition

The 2D reduced pulses transition is described in this [document](2d-context/pulses-transition.md).

## 2D reduced actions distribution

For given $`(p,a) ∈ \bar{P}×\bar{A}`$, the 2D reduced actions distribution is the function $`π^*`$ defined as $`\prod_{i=1}^N τ_a^*([p]_i,[a]_i)`$, where $`τ_{a}^*`$ is the function

$`\begin{aligned}
   τ_{a}^*:Q_4×Δ^*   &↦ \mathbb{Q}_+ \\
   (e,α)             &↦ \begin{cases}
                           [e]_1 &\text{if } α=a^+_x \\
                           [e]_2 &\text{if } α=a^-_x \\
                           [e]_3 &\text{if } α=a^+_y \\
                           [e]_4 &\text{if } α=a^-_y \\
                           1-\sum_{i=1}^4 [e]_i &\text{if } α=a_0
                        \end{cases}
\end{aligned}`$

***
[Nokton theory](../README.md)
