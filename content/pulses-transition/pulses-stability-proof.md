# Pulses stability proof

$`∀`$ $`1 ≤ i ≤ N`$,

$`\sum_{j=1}^6 [[λ_{in}(t,s,p)]_i]_j=\sum_{j=1}^6 [\frac{1}{1+\lvert{τ_{in}(t,s,c^*,[s]_i,[c^*]_i)}\rvert}([p]_i+τ_{in}(t,s,c^*,[s]_i,[c^*]_i))]_j`$ =

$`\frac{1}{1+\lvert{τ_{in}(t,s,c^*,[s]_i,[c^*]_i)}\rvert} \sum_{j=1}^6 [[p]_i+τ_{in}(t,s,c^*,[s]_i,[c^*]_i)]_j`$ =

$`\frac{1}{1+\lvert{τ_{in}(t,s,c^*,[s]_i,[c^*]_i)}\rvert} (\sum_{j=1}^6 [[p]_i]_j+ \sum_{j=1}^6 [τ_{in}(t,s,c^*,[s]_i,[c^*]_i)]_j)`$ =

$`\frac{1}{1+\lvert{τ_{in}(t,s,c^*,[s]_i,[c^*]_i)}\rvert} (\sum_{j=1}^6 [[p]_i]_j+ \lvert{τ_{in}(t,s,c^*,[s]_i,[c^*]_i)}\rvert)`$.

if $`[p]_i ∈ Q_6`$ i.e $`\sum_{j=1}^6 [[p]_i]_j ≤ 1`$ then $`\sum_{j=1}^6 [[p]_i]_j + \lvert{τ_{in}(t,s,c^*,[s]_i,[c^*]_i)}\rvert ≤ 1 + \lvert{τ_{in}(t,s,c^*,[s]_i,[c^*]_i)}\rvert`$ so

$`\frac{1}{1+\lvert{τ_{in}(t,s,c^*,[s]_i,[c^*]_i)}\rvert} \sum_{j=1}^6 [[p]_i]_j + \lvert{τ_{in}(t,s,c^*,[s]_i,[c^*]_i)}\rvert ≤ 1`$ ⟹ $`\sum_{j=1}^6 [[λ_{in}(t,s,p)]_i]_j ≤ 1`$ so $`[λ_{in}(t,s,p)]_i ∈ Q_6`$.

***
[Pulses transition](../pulses-transition.md)
