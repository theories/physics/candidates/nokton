# Pulses transition

We define these functions :

$`\begin{aligned}
   \hat{ϵ}:\mathbb{Z}   &↦ \mathbb{N} \\
   z                    &↦ \begin{cases}
                            0 &\text{if } z<0 \\
                            z &\text{else}
                        \end{cases}
\end{aligned}`$

$`\begin{aligned}
   \bar{ϵ}:Z_3 &↦ \mathbb{N} \\
   (x,y,z)     &↦ \begin{cases}
                     1 &\text{if } x=0, y=0 \text{ and } z=0 \\
                     x^2+y^2+z^2 &\text{else}
                  \end{cases}
\end{aligned}`$

$`\begin{aligned}
   ρ:C   &↦ \{-1,0,1\} \\
   c     &↦ \begin{cases}
               -1 &\text{if } c=c_- \\
               0 &\text{if } c=c_0 \\
               1 &\text{if } c=c_+
            \end{cases}
\end{aligned}`$

$`\begin{aligned}
   K:\mathbb{Q}_+^6 &↦ \mathbb{Q}_+ \\
   e                &↦ \sum_{i=1}^6 [e]_i
\end{aligned}`$

## Electric coupling

We note $`ρ(c)=\bar{c}`$ and we define electric coupling $`c_e`$ as :

$`\begin{aligned}
   c_e:Z_3×C×Z_3×C   &↦ \mathbb{Q}_+^6 \\
   (r_1,c_1,r_2,c_2) &↦ \frac{H_e}{\bar{ϵ}(r_1-r_2)}
                        (\hat{ϵ}(\bar{c_1}\bar{c_2}[r_2-r_1]_1),\hat{ϵ}(\bar{c_1}\bar{c_2}[r_1-r_2]_1),
                        \hat{ϵ}(\bar{c_1}\bar{c_2}[r_2-r_1]_2),\hat{ϵ}(\bar{c_1}\bar{c_2}[r_1-r_2]_2),
                        \hat{ϵ}(\bar{c_1}\bar{c_2}[r_2-r_1]_3),\hat{ϵ}(\bar{c_1}\bar{c_2}[r_1-r_2]_3))
\end{aligned}`$

## Gravitational coupling

We define gravitational coupling $`c_g`$ as :

$`\begin{aligned}
   c_g:Z_3×Z_3 &↦ \mathbb{Q}_+^6 \\
   (r_1,r_2)   &↦ \frac{H_g}{\bar{ϵ}(r_1-r_2)}
                  (\hat{ϵ}([r_1-r_2]_1),\hat{ϵ}([r_2-r_1]_1),
                  \hat{ϵ}([r_1-r_2]_2),\hat{ϵ}([r_2-r_1]_2),
                  \hat{ϵ}([r_1-r_2]_3),\hat{ϵ}([r_2-r_1]_3))
\end{aligned}`$

## Pulses transition function

We note $`K(e)=\lvert{e}\rvert`$.
For given $`(t,s,p) ∈ \mathbb{N^*}×S×P`$ the pulses transition is the function $`λ_{in}`$ defined as $`∀`$ $`1 ≤ i ≤ N`$

$`[λ_{in}(t,s,p)]_i=\frac{1}{1+\lvert{τ_{in}(t,s,c^*,[s]_i,[c^*]_i)}\rvert}([p]_i+τ_{in}(t,s,c^*,[s]_i,[c^*]_i))`$

where the pulse contribution $`τ_{in}`$ is the function

$`\begin{aligned}
   τ_{in}:\mathbb{N^*}×S×C^N×Z_3×C   &↦ \mathbb{Q}_+^6 \\
   (t,s,q,r^*,q^*)                   &↦ λ_f(t,r^*,q^*)+\sum_{i=1}^N c_e([s]_i,[q]_i,r^*,q^*)+c_g([s]_i,r^*)
\end{aligned}`$

In this [document](pulses-transition/pulses-stability-proof.md) we prove that $`∀`$ $`1 ≤ i ≤ N`$, if $`[p]_i ∈ Q_6`$ then $`[λ_{in}(t,s,p)]_i ∈ Q_6`$.

***
[Nokton theory](../README.md)
